import React from 'react';
import styled from 'styled-components';
import { Container } from '../view/Containers';
import { colors } from '../view/colors';
import { Paragraph } from '../view/Paragraph';
import { Headers } from '../view/TextSubHeaders';
import { RoundedTwine } from '../view/Buttons';

const KnowledgeContainer = styled(Container)`
  margin-top: 60px;
  background: ${colors.gallery};
  padding: 185px 0;
`;

const KnowledgeBox = styled.div`
  margin: 0 auto;
  background: white;
  max-width: 710px;
  min-height: 452px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const GoldenSeparator = styled.hr`
  margin: 11px 0;
  height: 1px;
  border: none;
  border-bottom: 1px solid ${colors.twine};
  width: 64px;
`;

const Article = styled.article`
  max-width: 414px;
  margin-bottom: 28px;
`;

const ArticleParagraph = styled(Paragraph)`
  text-align: center;
`;

const BoxParagraph = styled(Paragraph)`
  letter-spacing: 0.2em;
`;

const Header = styled(Headers)`
  margin: 0;
  font-size: 40px;
`;

interface Props {
  subHeader?: string;
  header: string;
  children: string | JSX.Element;
  buttonText: string;
  onClick: () => void;
}

export function BorderSquareSection({
  subHeader,
  header,
  children,
  buttonText,
  onClick,
}: Props): JSX.Element {
  return (
    <section>
      <KnowledgeContainer>
        <KnowledgeBox>
          {subHeader && (
            <BoxParagraph>
              {subHeader}
            </BoxParagraph>
          )}
          <Header as="h2">
            {header}
          </Header>
          <GoldenSeparator />
          <Article>
            <ArticleParagraph>
              {children}
            </ArticleParagraph>
          </Article>
          <RoundedTwine
            onClick={onClick}
          >
            {buttonText}
          </RoundedTwine>
        </KnowledgeBox>
      </KnowledgeContainer>
    </section>
  )
}
