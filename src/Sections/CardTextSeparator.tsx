import React from 'react';
import styled from 'styled-components';
import { colors } from './../view/colors';
import { Paragraph, HighlightedParagraph } from './../view/Paragraph';

interface Props {
  header: string;
  text: string;
}

const GoldenSeparator = styled.hr`
  height: 1px;
  border: none;
  border-bottom: 1px solid ${colors.twine};
  width: 64px;
`;

const CardParagraph = styled(Paragraph)`
  text-align: center;
  text-transform: uppercase;
  margin-top: 0;
  margin-bottom: 0;
  letter-spacing: 0.2em;
`;

const CardTextSeparatorSection = styled.section`
  margin: 80px auto;
  max-width: 606px;
`;

const CardText = styled(HighlightedParagraph)`
  margin-top: 14px;
  margin-bottom: 21px;
  font-size: 20px;
  line-height: 27px;
  text-align: center;
`;

export const CardTextSeparator = ({ header, text }: Props): JSX.Element => (
  <CardTextSeparatorSection>
    <CardParagraph>
      {header}
    </CardParagraph>
    <CardText>
      {text}
    </CardText>
    <GoldenSeparator />
  </CardTextSeparatorSection>
)
