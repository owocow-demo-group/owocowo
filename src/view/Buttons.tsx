/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled, { css } from 'styled-components';
import { colors } from './colors';


interface Props {
  children: JSX.Element | string;
  onClick: () => void;
}

const Button = ({ children, onClick, ...props }: Props): JSX.Element => (
  <button
    onClick={onClick}
    {...props}
    type="button"
  >
    {children}
  </button>
);

const buttonBase = css`
  font-family: 'Roboto', sans-serif;
  text-transform: uppercase;
  background: transparent;
  border: none;
  letter-spacing: 0.06em;
  box-shadow: none;
  font-weight: 500;
  outline: none;
  transition: box-shadow 0.5s;
`;

const RoundedButtons = css`
  ${buttonBase}
  box-sizing: border-box;
  border-radius: 25px;
  margin: 0 5px;
  height: 46px;
  padding: 0 30px;
  font-size: 18px;
`;

export const RoundedWhite = styled(Button)`
  ${RoundedButtons}
  background: white;
  color: ${colors.twine};
  &:hover {
    box-shadow: inset 0px 0px 6px 0px ${colors.twine};
  }
`;

export const RoundedTransparent = styled(Button)`
  ${RoundedButtons}
  border: 2px solid white;
  color: white;
  &:hover {
    box-shadow: inset 0px 0px 6px 0px white;
  }
`;

export const RoundedTwine = styled(Button)`
  ${RoundedButtons}
  border: 2px solid ${colors.twine};
  color: ${colors.twine};
  &:hover {
    box-shadow: inset 0px 0px 6px 0px ${colors.twine};
  }
`;
