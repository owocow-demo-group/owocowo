import React from 'react';
import styled from 'styled-components';

const Header = styled.span`
  font-family: 'Playfair Display', serif;
`;

export const Headers = (as: any, children: string | JSX.Element | JSX.Element[]): JSX.Element => (
  <Header as={as}>
    {children}
  </Header>
);
