import React from "react";
import styled from 'styled-components';

const P = styled.p`
  font-family: 'Roboto', sans-serif;
`;

const HighlightedP = styled.p`
  font-family: 'Playfair Display', serif;
  font-weight: bold;
`;

interface Props {
  children: string | JSX.Element | JSX.Element[];
}

export const Paragraph = ({ children, ...props }: Props) => <P {...props}>{children}</P>
export const HighlightedParagraph = ({ children, ...props }: Props) => <HighlightedP {...props}>{children}</HighlightedP>
