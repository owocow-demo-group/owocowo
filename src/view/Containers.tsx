
import React from 'react';
import styled from 'styled-components';

const StyledContainer = styled.div`
  margin: 0 auto;
  max-width: 1080px;
`;

interface Props {
  children: JSX.Element | JSX.Element[] | string;
}

export const Container = ({ children, ...props }: Props): JSX.Element => (
  <StyledContainer
    {...props}
  >
    {children}
  </ StyledContainer>
);
