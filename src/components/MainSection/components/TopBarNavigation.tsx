import React from 'react';
import styled from 'styled-components';
import HeaderLogo from '../../../svg/HeaderLogo.svg';
import { Link } from 'react-router-dom';

const HeaderBox = styled.nav`
  width: 100%;
  height: 244px;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

const HeaderText = styled(Link)`
  color: #fff;
  font-size: 20px;
  font-style: normal;
  font-size: 20px;
`;

const HeaderImg = styled.img`
  max-height: 244px;
  @media screen and (max-width: 1439px) {
    max-height: 150px;
  }
`;

export function TopBarNavigation(): JSX.Element {
  return (
    <HeaderBox>
      <HeaderText to='/Knowledge'>O Nas</HeaderText>
      <HeaderText to='/'>Uprawy</HeaderText>
      <HeaderText to='/'>Ekologia</HeaderText>
      <HeaderImg alt="logo" src={HeaderLogo} />
      <HeaderText to='/'>Blog</HeaderText>
      <HeaderText to='/'>Zespół</HeaderText>
      <HeaderText to='/'>Kontakt</HeaderText>
    </HeaderBox>
  );
}
