import React from 'react';
import { BorderSquareSection } from './../../../../Sections/BorderSquareSection';

export const KnowledgeSection = (): JSX.Element => (
  <BorderSquareSection
    subHeader="WIEDZA"
    header="Z natury"
    buttonText="Nasze uprawy"
    onClick={() => alert('just watch')}
  >
    <>
      Od pokoleń znamy się na owocach.
      <br />
    Używamy starych, sprawdzonych receptur,
      <br />
    naszych własnych owoców, a przetwory robimy ręcznie,
      <br />
    naszym zdaniem  jest to najlepszy sposób na zachowanie
    ich właściwosci prozdrowotnych.
    </>
  </BorderSquareSection>
)
