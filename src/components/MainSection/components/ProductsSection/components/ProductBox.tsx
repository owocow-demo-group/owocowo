import React, { useState } from 'react';
import styled, { CSSProp, css, keyframes } from 'styled-components';

interface Props {
  img: string;
  content: string;
  title: string;
}

const Box = styled.div`
  background: linear-gradient(
  180deg, rgba(0, 0, 0, 0) 39.82%,
  rgba(0, 0, 0, 0.4) 79.16%), 
  ${({ img }: { img: string }): CSSProp => `url(${img})`},
  #C4C4C4;
  background-position: center;
  position: relative;
  border-radius: 10px;
  width: 340px;
  max-width: 340px;
  height: 340px;
`;

const moveCotentAnimation = keyframes`
  from { 
    height: 0; 
    padding: 0 45px; 
  }
  to { 
    height: 100px;
    padding: 37px 45px;
  }
`;

const closeCotentAnimation = keyframes`
  from { 
    height: 100px; 
    padding: 37px 45px;
  }
  to { 
    height: 0; 
    padding: 0 45px; 
  }
`;

const BoxTitle = styled.p`
  text-align: center;
  width: 100%;
  font-size: 30px;
  color: white;
  margin: 0;
  padding-bottom: 20px;
  font-family: 'Playfair Display',serif;
  font-style: normal;
  background: linear-gradient(180deg, rgba(0, 0, 0, 0) 68.88%, rgba(0, 0, 0, 0.4) 87.33%), transparent;
`;

const BoxInner = styled.div`
  overflow: hidden;
  position: absolute;
  bottom: 0;
  width: 100%;
`;

const BoxContent = styled.div`
  text-align: center;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
  background-color: #DADADA;
  transition: height 1s;
  height: 0;

  ${({ hovered }: { hovered: boolean }): CSSProp => (hovered ? css`
    animation: ${moveCotentAnimation} .5s ease forwards;
  ` : css`
    animation: ${closeCotentAnimation} .5s ease forwards;
  `)}
`;
export function ProductBox({ img, content, title }: Props): JSX.Element {
  const [hovered, setHovered] = useState(false);
  return (
    <Box
      onMouseEnter={(): void => setHovered(true)}
      onMouseLeave={(): void => setHovered(false)}
      img={img}
    >
      <BoxInner>
        <BoxTitle>
          {title}
        </BoxTitle>
        <BoxContent hovered={hovered}>
          {content}
        </BoxContent>
      </BoxInner>
    </Box>
  );
}
