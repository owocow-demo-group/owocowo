import React from 'react';
import styled from 'styled-components';
import { Headers } from '../../../../view/TextSubHeaders';
import { ProductBox } from './components/ProductBox';
import products1 from '../../../../img/products1.jpg';
import products2 from '../../../../img/products2.jpg';
import products3 from '../../../../img/products3.jpg';
import { Container } from '../../../../view/Containers';

interface Product {
  img: string;
  content: string;
  title: string;
}

const ProductHeader = styled(Headers)`
  letter-spacing: 0.06em;
  font-size: 40px;
  line-height: 53px;
  font-family: 'Playfair Display', serif;
  font-weight: bold;
  text-align: center;
`;

const ProductListContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const productList = [
  {
    img: products1,
    title: 'Świeże soki',
    content: 'lorem ipsum dolor sit amet consectetur adipiscing elit',
  },
  {
    img: products2,
    title: 'Oleje tłoczone',
    content: 'lorem ipsum dolor sit amet consectetur adipiscing elit',
  },
  {
    img: products3,
    title: 'Przetwory',
    content: 'lorem ipsum dolor sit amet consectetur adipiscing elit',
  },
];

export function ProductsSection(): JSX.Element {
  return (
    <Container>
      <ProductHeader as="h2">
        Produkty
      </ProductHeader>
      <ProductListContainer>
        {productList.map((product: Product): JSX.Element => (
          <ProductBox
            key={product.content}
            title={product.title}
            img={product.img}
            content={product.content}
          />
        ))}
      </ProductListContainer>
    </Container>
  );
}
