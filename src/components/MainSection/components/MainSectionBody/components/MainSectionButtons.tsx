import React from 'react';
import { RoundedTransparent, RoundedWhite } from '../../../../../view/Buttons';

export function MainSectionButtons(): JSX.Element {
  return (
    <>
      <RoundedWhite>
        POZNAJ NAS
      </RoundedWhite>
      <RoundedTransparent>
        Poznaj PRODUKTY
      </RoundedTransparent>
    </>
  );
}
