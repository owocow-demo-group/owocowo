import YouTube from '../../../../../../svg/YouTube.svg';
import Facebook from '../../../../../../svg/Facebook.svg';
import Instagram from '../../../../../../svg/Instagram.svg';

export const socials = [
  {
    icon: Facebook,
    link: 'https://www.facebook.com/owocowo.zdrowie.z.natury/',
  },
  {
    icon: Instagram,
    link: 'https://www.instagram.com/explore/locations/312496356213758/paulina-chruslinska-owocowo/',
  },
  {
    icon: YouTube,
    link: 'https://www.youtube.com/channel/UC7Ygos3v7DwE32moHlQwr2A',
  },
];
