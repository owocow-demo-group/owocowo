import React from 'react';
import styled from 'styled-components';
import { socials } from './utils';

const SectionSocialMediaWrapper = styled.section`
  display: flex;
  justify-content: flex-end;
  margin: 0 30px;
  margin-top: 20px;
`;

const SocialLink = styled.a`
  color: #fff;
  justify-content: center;
  align-items: center;
  display: flex;
  text-decoration: none;
  margin: 0 20px;
  height: 30px;
  width: 30px;
`;

export function SocialMediaSection(): JSX.Element {
  return (
    <SectionSocialMediaWrapper>
      {socials.map((detail): JSX.Element => (
        <>
          <SocialLink target="_blank" href={detail.link}>
            <img src={detail.icon} alt="social media" />
          </SocialLink>
        </>
      ))}
    </SectionSocialMediaWrapper>
  );
}
