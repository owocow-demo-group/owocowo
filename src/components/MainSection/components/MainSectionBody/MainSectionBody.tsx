import React from 'react';
import styled from 'styled-components';
import { Paragraph } from '../../../../view/Paragraph';
import { Headers } from '../../../../view/TextSubHeaders';
import { MainSectionButtons } from './components/MainSectionButtons';

const BodyWrapper = styled.div`
  max-width: 525px;
  margin: 0 auto;
  margin-top: 24px;
  text-align: center;
    @media screen and (min-width: 1440px) {
      margin-top: 80px;
    }
`;

const BodyParagraph = styled(Paragraph)`
  @media screen and (min-width: 1440px) {
    font-size: 26px;
  }
  font-size: 18px;
  color: #fff;
`;

const BodyHeader = styled(Headers)`
  margin-top: 0;
  font-size: 35px;
  @media screen and (min-width: 1440px) {
    font-size: 60px;
  }
  color: #fff;
`;

const SecctionButtonsWrapper = styled.div`
  margin-top: 56px;
`;

export function MainSectionBody(): JSX.Element {
  return (
    <BodyWrapper>
      <BodyHeader as="h1">
        Poznajmy się
      </BodyHeader>
      <BodyParagraph>
        <>
          Od pokoleń znamy się na owocach.
          <br />
          Używamy starych, sprawdzonych receptur,
          <br />
          naszych własnych owoców,
          a przetwory
          <br />
          robimy ręcznie,
          żeby zachować ich
          <br />
          właściwości
          prozdrowotne.
        </>
      </BodyParagraph>
      <SecctionButtonsWrapper>
        <MainSectionButtons />
      </SecctionButtonsWrapper>
    </BodyWrapper>
  );
}
