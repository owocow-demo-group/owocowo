import React from 'react';
import styled from 'styled-components';
import MainSectionImg from '../../img/MainSectionImg.jpg';
import { TopBarNavigation } from './components/TopBarNavigation';
import { MainSectionBody } from './components/MainSectionBody/MainSectionBody';
import { SocialMediaSection } from './components/MainSectionBody/components/SocialMediaSection/SocialMediaSection';
import { ProductsSection } from './components/ProductsSection/ProductsSection';
import { KnowledgeSection } from './components/KnowledgeSection/KnowledgeSection';
import { CardTextSeparator } from './../../Sections/CardTextSeparator';

const MainSectionBox = styled.section`
  min-height: 100vh;
  padding-bottom: 20px;
  padding-top: 9px;
  background: linear-gradient(90.12deg, rgba(0, 0, 0, 0) 14.57%, 
  rgba(0, 0, 0, 0.2) 49.35%, 
  rgba(0, 0, 0, 0) 87.49%), 
  url(${MainSectionImg}),
  #C4C4C4;
  background-attachment: fixed;
  background-repeat: no-repeat;
  background-size: cover;
  display: flex;
  flex-direction: column;
`;

const separatorText = `
Owocowo to wyjątkowe smaki i aromaty dawnych receptur,
szczelnie zamknięte w szklanych słoiczkach i butelkach
`;

export function MainSection(): JSX.Element {
  return (
    <>
      <MainSectionBox>
        <TopBarNavigation />
        <MainSectionBody />
        <SocialMediaSection />
      </MainSectionBox>
      <ProductsSection />
      <KnowledgeSection />
      <CardTextSeparator
        header="w skrócie"
        text={separatorText}
      />
    </>
  );
}
