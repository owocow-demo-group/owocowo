import React from 'react';
import { Switch, Route, BrowserRouter as Router, } from 'react-router-dom';
import { MainSection } from './MainSection/MainSection';
import '../CSSRules/normalize.css';

export function App(): JSX.Element {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/">
            <MainSection />
          </Route>
          <Route exact route='/Knowledge'>
            <>
              lorem ipsum olor sita amet
            </>
          </Route>
        </Switch>
        <p>Footer</p>
      </Router>
    </>
  );
}
