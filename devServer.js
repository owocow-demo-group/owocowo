const ParcelProxyServer = require('parcel-proxy-server');
const Path = require('path');

const entryFiles = Path.join(__dirname, 'src', 'index.html');

// configure the proxy server
const server = new ParcelProxyServer({
  entryPoint: entryFiles,
  parcelOptions: {
    open: true,
    https: true,
  },
});

// start up the server
server.listen(8080, () => console.log('i run on port 8080'));
